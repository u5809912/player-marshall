package Services.Database;

import NickSifniotis.DatabaseManager.DBManager;
import java.sql.SQLException;


/**
 * Created by Nick Sifniotis on 1/10/16.
 *
 * Helper functions for the database service.
 */
public class DBHelper
{
    public static void onCreate(DBManager db)
    {
        try
        {
            db.Execute(DB.Game.CreateSQL);
            db.Execute(DB.GameType.CreateSQL);
            db.Execute(DB.PlayerSubmission.CreateSQL);
            db.Execute(DB.PointStructure.CreateSQL);
            db.Execute(DB.Score.CreateSQL);
            db.Execute(DB.Tournament.CreateSQL);
        }
        catch (SQLException e)
        {
            onError (e.getMessage());
        }
    }


    /**
     * Database schema has changed, or we're forcing a reset, so delete the tables
     * and recreate them.
     *
     * @param db The database manager in use.
     */
    public static void onUpgrade(DBManager db)
    {
        try
        {
            db.Execute(DB.Game.DestroySQL);
            db.Execute(DB.GameType.DestroySQL);
            db.Execute(DB.PlayerSubmission.DestroySQL);
            db.Execute(DB.PointStructure.DestroySQL);
            db.Execute(DB.Score.DestroySQL);
            db.Execute(DB.Tournament.DestroySQL);
        }
        catch (SQLException e)
        {
            onError (e.getMessage());
        }

        onCreate(db);
    }


    public static void onError(String s)
    {
        // todo sort this out. Send a message to the log service?
    }
}
