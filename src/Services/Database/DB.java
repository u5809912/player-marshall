package Services.Database;


/**
 * Created by Nick Sifniotis on 1/10/16.
 *
 * Implementation of database controls in the style of the Android development setup.
 */
public class DB
{
    public static boolean UPDATE_DB = true;

    public static class GameType
    {
        public static final String TABLE_NAME = "_game_type";

        public static final String _ID = "id";
        public static final String GAME_NAME = "name";
        public static final String ENGINE_CLASS = "engine_class";
        public static final String VIEWER_CLASS = "viewer_class";
        public static final String USES_VIEWER = "uses_viewer";
        public static final String NUM_PLAYERS = "num_players";

        public static final String CreateSQL = "CREATE TABLE " + TABLE_NAME + " ("
                + _ID + " INTEGER PRIMARY KEY, "
                + GAME_NAME + " TEXT, "
                + ENGINE_CLASS + " TEXT, "
                + VIEWER_CLASS + " TEXT, "
                + USES_VIEWER + " INTEGER, "
                + NUM_PLAYERS + " INTEGER)";

        public static final String DestroySQL = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }

    public static class Game
    {
        public static final String TABLE_NAME = "_game";

        public static final String _ID = "id";
        public static final String ROUND_NUMBER = "round_number";
        public static final String GAME_NUMBER = "game_number";
        public static final String TOURNAMENT_ID = "tournament_id";

        public static final String CreateSQL = "CREATE TABLE " + TABLE_NAME + " ("
                + _ID + " INTEGER PRIMARY KEY, "
                + ROUND_NUMBER + " INTEGER, "
                + GAME_NUMBER + " INTEGER, "
                + TOURNAMENT_ID + " INTEGER)";
        public static final String DestroySQL = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }

    public static class PlayerSubmission
    {
        public static final String TABLE_NAME = "_player_submission";

        public static final String _ID = "id";
        public static final String TOURNAMENT_ID = "tournament_id";
        public static final String NAME = "name";
        public static final String EMAIL = "email";
        public static final String USES_AVATAR = "uses_avatar";
        public static final String PLAYING = "playing";
        public static final String DISQUALIFIED = "disqualified";
        public static final String RETIRED = "retired";

        public static final String CreateSQL = "CREATE TABLE " + TABLE_NAME + " ("
                + _ID + " INTEGER PRIMARY KEY, "
                + TOURNAMENT_ID + " INTEGER, "
                + NAME + " TEXT, "
                + EMAIL + " TEXT, "
                + USES_AVATAR + " INTEGER, "
                + PLAYING + " INTEGER, "
                + DISQUALIFIED + " INTEGER, "
                + RETIRED + " INTEGER)";
        public static final String DestroySQL = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }

    public static class PointStructure
    {
        public static final String TABLE_NAME = "_point_structure";

        public static final String _ID = "id";
        public static final String TOURNAMENT_ID = "tournament_id";
        public static final String POSITION = "position";
        public static final String POINTS = "points";

        public static final String CreateSQL = "CREATE TABLE " + TABLE_NAME + " ("
                + _ID + " INTEGER PRIMARY KEY, "
                + TOURNAMENT_ID + " INTEGER, "
                + POSITION + " INTEGER, "
                + POINTS + " INTEGER)";
        public static final String DestroySQL = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }

    public static class Score
    {
        public static final String TABLE_NAME = "_score";

        public static final String _ID = "id";
        public static final String SUBMISSION_ID = "submission_id";
        public static final String GAME_ID = "game_id";
        public static final String SCORE = "score";
        public static final String NO_SCORE = "no_score";
        public static final String DISQUALIFIED = "disqualified";

        public static final String CreateSQL = "CREATE TABLE " + TABLE_NAME + " ("
                + _ID + " INTEGER PRIMARY KEY, "
                + SUBMISSION_ID + " INTEGER, "
                + GAME_ID + " INTEGER, "
                + SCORE + " INTEGER, "
                + NO_SCORE + " INTEGER, "
                + DISQUALIFIED + " INTEGER)";
        public static final String DestroySQL = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }

    public static class Tournament
    {
        public static final String TABLE_NAME = "_tournament";

        public static final String _ID = "id";
        public static final String NAME = "name";
        public static final String GAME_ON = "game_on";
        public static final String TIMEOUT = "timeout";
        public static final String ALLOW_RESUBMIT = "allow_resubmit";
        public static final String ALLOW_SUBMIT = "allow_submit";
        public static final String USE_NULL_MOVES = "use_null_moves";
        public static final String GAME_TYPE_ID = "game_type_id";
        public static final String NUM_PLAYERS = "num_players";
        public static final String PLAYER_INTERFACE_CLASS = "player_interface";
        public static final String VERIFICATION_CLASS = "verification";

        public static final String CreateSQL = "CREATE TABLE " + TABLE_NAME + " ("
                + _ID + " INTEGER PRIMARY KEY, "
                + NAME + " TEXT, "
                + GAME_ON + " INTEGER, "
                + TIMEOUT + " INTEGER, "
                + ALLOW_RESUBMIT + " INTEGER, "
                + ALLOW_SUBMIT + " INTEGER, "
                + USE_NULL_MOVES + " INTEGER, "
                + GAME_TYPE_ID + " INTEGER, "
                + NUM_PLAYERS + " INTEGER, "
                + PLAYER_INTERFACE_CLASS + " TEXT, "
                + VERIFICATION_CLASS + " TEXT)";
        public static final String DestroySQL = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }
}
