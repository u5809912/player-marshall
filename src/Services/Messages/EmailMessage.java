package Services.Messages;

import Common.Email.EmailTypes;


/**
 * Created by Nick Sifniotis on 5/10/15.
 *
 * The 'envelope' that contains the email to be mailed out.
 *
 */
public class EmailMessage extends Message
{
    private String destination;
    private EmailTypes type;
    private int tournament_id;
    private String attachment;


    /**
     * A couple of simple constructors.
     */
    public EmailMessage(EmailTypes type, String dest, int tourney_id)
    {
        this (type, dest, tourney_id, null);
    }

    public EmailMessage(EmailTypes type, String dest, int tournament_id, String attachment)
    {
        this.type = type;
        this.destination = dest;
        this.tournament_id = tournament_id;
        this.attachment = attachment;
    }


    /**
     * Accessor functions.
     *
     * @return Various bits of data.
     */
    public String Destination() { return this.destination; }
    public int TournamentID() { return this.tournament_id; }
    public EmailTypes EmailType() { return this.type; }
    public String Attachment() { return this.attachment; }
}
