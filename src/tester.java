import Common.NewDataModel.GameType;
import NickSifniotis.SimpleDatabase.SimpleDB;


/**
 * Created by Nick Sifniotis on 15/10/15.
 *
 * Modified on 1st October 2016.
 */
public class tester
{
    public static void main(String[] args)
    {
        SimpleDB.Initialise("testing.db", "database");

        SimpleDB.CreateTable(GameType.class);

        GameType game = (GameType) SimpleDB.New(GameType.class);
        game.name.Value = "This is a test";
        SimpleDB.Save (game);


    }
}
